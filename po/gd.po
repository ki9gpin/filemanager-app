# Gaelic; Scottish translation for ubuntu-filemanager-app
# Copyright (c) 2013 Rosetta Contributors and Canonical Ltd 2013
# This file is distributed under the same license as the ubuntu-filemanager-app package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2013.
# GunChleoc <fios@foramnagaidhlig.net>, 2014, 2016.
msgid ""
msgstr ""
"Project-Id-Version: ubuntu-filemanager-app\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-07-03 13:35+0000\n"
"PO-Revision-Date: 2016-09-15 14:46+0000\n"
"Last-Translator: GunChleoc <Unknown>\n"
"Language-Team: Fòram na Gàidhlig\n"
"Language: gd\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n==1 || n==11) ? 0 : (n==2 || n==12) ? 1 : "
"(n > 2 && n < 20) ? 2 : 3;\n"
"X-Launchpad-Export-Date: 2017-04-08 06:04+0000\n"
"X-Generator: Launchpad (build 18343)\n"

#: ../src/app/qml/actions/AddBookmark.qml:6
msgid "Add bookmark"
msgstr ""

#: ../src/app/qml/actions/ArchiveExtract.qml:6
#: ../src/app/qml/dialogs/OpenArchiveDialog.qml:18
#: ../src/app/qml/dialogs/OpenWithDialog.qml:33
msgid "Extract archive"
msgstr "Dì-dhùmhlaich an tasg-lann"

#: ../src/app/qml/actions/Delete.qml:5
#: ../src/app/qml/dialogs/ConfirmMultipleDeleteDialog.qml:14
#: ../src/app/qml/dialogs/ConfirmSingleDeleteDialog.qml:15
#: ../src/app/qml/panels/SelectionBottomBar.qml:26
msgid "Delete"
msgstr "Sguab às"

#: ../src/app/qml/actions/FileClearSelection.qml:8
msgid "Clear clipboard"
msgstr "Falamhaich an stòr-bhòrd"

#: ../src/app/qml/actions/FileCopy.qml:5
#: ../src/app/qml/panels/SelectionBottomBar.qml:45
msgid "Copy"
msgstr "Dèan lethbhreac"

#: ../src/app/qml/actions/FileCut.qml:5
#: ../src/app/qml/panels/SelectionBottomBar.qml:60
msgid "Cut"
msgstr "Gearr às"

#: ../src/app/qml/actions/FilePaste.qml:30
#, fuzzy, qt-format
msgid "Paste %1 file"
msgid_plural "Paste %1 files"
msgstr[0] "Cuir ann %1 fhaidhle"
msgstr[1] "Cuir ann %1 fhaidhle"
msgstr[2] "Cuir ann %1 faidhlichean"
msgstr[3] "Cuir ann %1 faidhle"

#: ../src/app/qml/actions/GoBack.qml:5
msgid "Go back"
msgstr ""

#: ../src/app/qml/actions/GoNext.qml:5
msgid "Go next"
msgstr ""

#: ../src/app/qml/actions/GoTo.qml:6
msgid "Go To"
msgstr "Rach gu"

#: ../src/app/qml/actions/NewItem.qml:6
msgid "New Item"
msgstr ""

#: ../src/app/qml/actions/PlacesBookmarks.qml:6
#: ../src/app/qml/ui/PlacesPage.qml:16
msgid "Places"
msgstr "Àiteachan"

#: ../src/app/qml/actions/Properties.qml:6
#: ../src/app/qml/dialogs/OpenWithDialog.qml:54
msgid "Properties"
msgstr "Roghainnean"

#: ../src/app/qml/actions/Rename.qml:5
#: ../src/app/qml/dialogs/ConfirmRenameDialog.qml:20
msgid "Rename"
msgstr "Thoir ainm ùr air"

#: ../src/app/qml/actions/Select.qml:5
#: ../src/app/qml/ui/FolderListPagePickModeHeader.qml:60
msgid "Select"
msgstr "Tagh"

#: ../src/app/qml/actions/SelectUnselectAll.qml:6
#, fuzzy
msgid "Select None"
msgstr "Tagh"

#: ../src/app/qml/actions/SelectUnselectAll.qml:6
#, fuzzy
msgid "Select All"
msgstr "Tagh"

#: ../src/app/qml/actions/Settings.qml:6
msgid "Settings"
msgstr "Roghainnean"

#: ../src/app/qml/actions/Share.qml:5
msgid "Share"
msgstr ""

#: ../src/app/qml/actions/TabsAdd.qml:5
msgid "Add tab"
msgstr ""

#: ../src/app/qml/actions/TabsCloseThis.qml:5
msgid "Close this tab"
msgstr "Dùin an taba seo"

#: ../src/app/qml/actions/TabsOpenInNewTab.qml:5
msgid "Open in a new tab"
msgstr "Fosgail ann an taba ùr"

#: ../src/app/qml/actions/UnlockFullAccess.qml:6
msgid "Unlock"
msgstr ""

#: ../src/app/qml/authentication/FingerprintDialog.qml:26
#: ../src/app/qml/authentication/PasswordDialog.qml:43
#: ../src/app/qml/dialogs/NetAuthenticationDialog.qml:28
msgid "Authentication required"
msgstr "Tha feum air dearbhadh"

#: ../src/app/qml/authentication/FingerprintDialog.qml:27
msgid "Use your fingerprint to access restricted content"
msgstr ""

#. TRANSLATORS: "Touch" here is a verb
#: ../src/app/qml/authentication/FingerprintDialog.qml:55
msgid "Touch sensor"
msgstr ""

#: ../src/app/qml/authentication/FingerprintDialog.qml:60
#, fuzzy
msgid "Use password"
msgstr "Sàbhail am facal-faire"

#: ../src/app/qml/authentication/FingerprintDialog.qml:69
#: ../src/app/qml/authentication/PasswordDialog.qml:80
#: ../src/app/qml/dialogs/CreateItemDialog.qml:60
#: ../src/app/qml/dialogs/ExtractingDialog.qml:35
#: ../src/app/qml/dialogs/FileActionDialog.qml:45
#: ../src/app/qml/dialogs/FileOperationProgressDialog.qml:44
#: ../src/app/qml/dialogs/NetAuthenticationDialog.qml:96
#: ../src/app/qml/dialogs/OpenArchiveDialog.qml:38
#: ../src/app/qml/dialogs/OpenWithDialog.qml:64
#: ../src/app/qml/dialogs/templates/ConfirmDialog.qml:41
#: ../src/app/qml/dialogs/templates/ConfirmDialogWithInput.qml:60
#: ../src/app/qml/ui/FileDetailsPopover.qml:184
#: ../src/app/qml/ui/FolderListPagePickModeHeader.qml:51
#: ../src/app/qml/ui/FolderListPageSelectionHeader.qml:26
msgid "Cancel"
msgstr "Sguir dheth"

#: ../src/app/qml/authentication/FingerprintDialog.qml:126
#, fuzzy
msgid "Authentication failed!"
msgstr "Dh’fhàillig an dearbhadh"

#: ../src/app/qml/authentication/FingerprintDialog.qml:137
msgid "Please retry"
msgstr ""

#: ../src/app/qml/authentication/PasswordDialog.qml:35
msgid "Authentication failed"
msgstr "Dh’fhàillig an dearbhadh"

#: ../src/app/qml/authentication/PasswordDialog.qml:45
#, fuzzy
msgid "Your passphrase is required to access restricted content"
msgstr ""
"Tha feum air facal-faire mus fhaighear cothrom air na faidhlichean air fad"

#: ../src/app/qml/authentication/PasswordDialog.qml:46
#, fuzzy
msgid "Your passcode is required to access restricted content"
msgstr ""
"Tha feum air facal-faire mus fhaighear cothrom air na faidhlichean air fad"

#: ../src/app/qml/authentication/PasswordDialog.qml:57
msgid "passphrase (default is 0000 if unset)"
msgstr ""

#: ../src/app/qml/authentication/PasswordDialog.qml:57
msgid "passcode (default is 0000 if unset)"
msgstr ""

#: ../src/app/qml/authentication/PasswordDialog.qml:69
#, fuzzy
msgid "Authentication failed. Please retry"
msgstr "Dh’fhàillig an dearbhadh"

#: ../src/app/qml/authentication/PasswordDialog.qml:74
#, fuzzy
msgid "Authenticate"
msgstr "Dh’fhàillig an dearbhadh"

#: ../src/app/qml/backend/FolderListModel.qml:123
#, fuzzy, qt-format
msgid "%1 file"
msgstr "%1 fhaidhle"

#: ../src/app/qml/backend/FolderListModel.qml:124
#, fuzzy, qt-format
msgid "%1 files"
msgstr "%1 fhaidhle"

#: ../src/app/qml/backend/FolderListModel.qml:135
#, fuzzy
msgid "Folder"
msgstr "Pasgan ùr"

#: ../src/app/qml/components/TextualButtonStyle.qml:48
#: ../src/app/qml/components/TextualButtonStyle.qml:56
#: ../src/app/qml/components/TextualButtonStyle.qml:63
msgid "Pick"
msgstr ""

#: ../src/app/qml/content-hub/FileOpener.qml:27
msgid "Open with"
msgstr "Fosgail le"

#: ../src/app/qml/dialogs/ConfirmExtractDialog.qml:15
msgid "Extract Archive"
msgstr "Dì-dhùmhlaich an tasg-lann"

#: ../src/app/qml/dialogs/ConfirmExtractDialog.qml:16
#, qt-format
msgid "Are you sure you want to extract '%1' here?"
msgstr ""
"A bheil thu cinnteach gu bheil thu airson “%1” a dhì-dhùmhlachadh an-seo?"

#: ../src/app/qml/dialogs/ConfirmMultipleDeleteDialog.qml:15
#: ../src/app/qml/dialogs/ConfirmSingleDeleteDialog.qml:16
#, qt-format
msgid "Are you sure you want to permanently delete '%1'?"
msgstr ""
"A bheil thu cinnteach gu bheil thu airson \"%1\" a sguabadh às gu buan?"

#: ../src/app/qml/dialogs/ConfirmMultipleDeleteDialog.qml:15
#, fuzzy
msgid "these files"
msgstr "Cuir ann faidhlichean"

#: ../src/app/qml/dialogs/ConfirmMultipleDeleteDialog.qml:18
#: ../src/app/qml/dialogs/ConfirmSingleDeleteDialog.qml:21
msgid "Deleting files"
msgstr "A’ sguabadh às nam faidhlichean"

#: ../src/app/qml/dialogs/ConfirmRenameDialog.qml:21
msgid "Enter a new name"
msgstr "Cuir a-steach ainm ùr"

#: ../src/app/qml/dialogs/CreateItemDialog.qml:13
#, fuzzy
msgid "Create Item"
msgstr "Cruthaich faidhle"

#: ../src/app/qml/dialogs/CreateItemDialog.qml:14
#, fuzzy
msgid "Enter name for new item"
msgstr "Cuir a-steach ainm an fhaidhle ùir"

#: ../src/app/qml/dialogs/CreateItemDialog.qml:18
#, fuzzy
msgid "Item name"
msgstr "Thoir ainm ùr air"

#: ../src/app/qml/dialogs/CreateItemDialog.qml:24
msgid "Create file"
msgstr "Cruthaich faidhle"

#: ../src/app/qml/dialogs/CreateItemDialog.qml:43
#, fuzzy
msgid "Create Folder"
msgstr "Cruthaich pasgan"

#: ../src/app/qml/dialogs/ExtractingDialog.qml:23
#, qt-format
msgid "Extracting archive '%1'"
msgstr "A’ dì-dhùmhlachadh na tasg-lainn “%1”"

#: ../src/app/qml/dialogs/ExtractingDialog.qml:44
#: ../src/app/qml/dialogs/NotifyDialog.qml:27
#: ../src/app/qml/dialogs/templates/ConfirmDialog.qml:31
#: ../src/app/qml/dialogs/templates/ConfirmDialogWithInput.qml:49
msgid "OK"
msgstr "Ceart ma-thà"

#: ../src/app/qml/dialogs/ExtractingDialog.qml:59
msgid "Extracting failed"
msgstr "Dh’fhàillig an dì-dhùmhlachadh"

#: ../src/app/qml/dialogs/ExtractingDialog.qml:60
#, qt-format
msgid "Extracting the archive '%1' failed."
msgstr "Dh’fhàillig dì-dhùmhlachadh na tasg-lainn “%1”."

#: ../src/app/qml/dialogs/FileActionDialog.qml:30
msgid "Choose action"
msgstr "Tagh gnìomh"

#: ../src/app/qml/dialogs/FileActionDialog.qml:31
#, qt-format
msgid "For file: %1"
msgstr "Airson an fhaidhle: %1"

#: ../src/app/qml/dialogs/FileActionDialog.qml:35
msgid "Open"
msgstr "Fosgail"

#: ../src/app/qml/dialogs/FileOperationProgressDialog.qml:27
msgid "Operation in progress"
msgstr "Tha obrachadh 'ga dhèanamh"

#: ../src/app/qml/dialogs/FileOperationProgressDialog.qml:29
msgid "File operation"
msgstr "Gnìomh faidhle"

#: ../src/app/qml/dialogs/NetAuthenticationDialog.qml:45
msgid "User"
msgstr "Cleachdaiche"

#: ../src/app/qml/dialogs/NetAuthenticationDialog.qml:58
msgid "Password"
msgstr "Facal-faire"

#: ../src/app/qml/dialogs/NetAuthenticationDialog.qml:72
msgid "Save password"
msgstr "Sàbhail am facal-faire"

#: ../src/app/qml/dialogs/NetAuthenticationDialog.qml:108
msgid "Ok"
msgstr "Ceart ma-thà"

#: ../src/app/qml/dialogs/OpenArchiveDialog.qml:8
msgid "Archive file"
msgstr "Faidhle tasg-lainn"

#: ../src/app/qml/dialogs/OpenArchiveDialog.qml:9
msgid "Do you want to extract the archive here?"
msgstr "A bheil thu airson an tasg-lann a dhì-dhùmhlachadh an-seo?"

#: ../src/app/qml/dialogs/OpenArchiveDialog.qml:28
#: ../src/app/qml/dialogs/OpenWithDialog.qml:44
msgid "Open with another app"
msgstr "Fosgail ann an aplacaid eile"

#: ../src/app/qml/dialogs/OpenWithDialog.qml:8
#, fuzzy
msgid "Open file"
msgstr "Fosgail le"

#: ../src/app/qml/dialogs/OpenWithDialog.qml:9
#, fuzzy
msgid "What do you want to do with the clicked file?"
msgstr "A bheil thu airson an tasg-lann a dhì-dhùmhlachadh an-seo?"

#: ../src/app/qml/dialogs/OpenWithDialog.qml:22
msgid "Preview"
msgstr ""

#: ../src/app/qml/filemanager.qml:170
#, qt-format
msgid "File %1"
msgstr "Faidhle %1"

#: ../src/app/qml/filemanager.qml:170
#, qt-format
msgid "%1 Files"
msgstr "%1 Faidhle"

#: ../src/app/qml/filemanager.qml:171
#, qt-format
msgid "Saved to: %1"
msgstr "Air a shàbhaladh gu: %1"

#: ../src/app/qml/panels/DefaultBottomBar.qml:24
msgid "Paste files"
msgstr "Cuir ann faidhlichean"

#: ../src/app/qml/ui/FileDetailsPopover.qml:34
msgid "Readable"
msgstr "Gabhaidh a leughadh"

#: ../src/app/qml/ui/FileDetailsPopover.qml:37
msgid "Writable"
msgstr "Gabhaidh sgrìobhadh ann"

#: ../src/app/qml/ui/FileDetailsPopover.qml:40
msgid "Executable"
msgstr "Gabhaidh a ruith"

#: ../src/app/qml/ui/FileDetailsPopover.qml:100
msgid "Where:"
msgstr ""

#: ../src/app/qml/ui/FileDetailsPopover.qml:127
#, fuzzy
msgid "Created:"
msgstr "Cruthaich pasgan"

#: ../src/app/qml/ui/FileDetailsPopover.qml:141
msgid "Modified:"
msgstr "Air atharrachadh:"

#: ../src/app/qml/ui/FileDetailsPopover.qml:155
msgid "Accessed:"
msgstr "Air inntrigeadh:"

#: ../src/app/qml/ui/FileDetailsPopover.qml:176
msgid "Permissions:"
msgstr "Ceadan:"

#: ../src/app/qml/ui/FolderListPage.qml:241
#: ../src/app/qml/ui/FolderListPage.qml:296
msgid "Restricted access"
msgstr ""

#: ../src/app/qml/ui/FolderListPage.qml:245
msgid ""
"Authentication is required in order to see all the content of this folder."
msgstr ""

#: ../src/app/qml/ui/FolderListPage.qml:282
msgid "No files"
msgstr "Chan eil faidhle ann"

#: ../src/app/qml/ui/FolderListPage.qml:283
msgid "This folder is empty."
msgstr ""

#: ../src/app/qml/ui/FolderListPage.qml:297
msgid "Authentication is required in order to see the content of this folder."
msgstr ""

#: ../src/app/qml/ui/FolderListPage.qml:313
msgid "File operation error"
msgstr "Mearachd le obrachadh faidhle"

#: ../src/app/qml/ui/FolderListPageDefaultHeader.qml:21
#: ../src/app/qml/ui/FolderListPagePickModeHeader.qml:21
#, fuzzy, qt-format
msgid "%1 item"
msgid_plural "%1 items"
msgstr[0] "nithean"
msgstr[1] "nithean"
msgstr[2] "nithean"
msgstr[3] "nithean"

#: ../src/app/qml/ui/FolderListPagePickModeHeader.qml:21
msgid "Save here"
msgstr ""

#: ../src/app/qml/ui/FolderListPageSelectionHeader.qml:18
#, qt-format
msgid "%1 item selected"
msgid_plural "%1 items selected"
msgstr[0] ""
msgstr[1] ""
msgstr[2] ""
msgstr[3] ""

#: ../src/app/qml/ui/ViewPopover.qml:24
msgid "Show Hidden Files"
msgstr "Seall faidhlichean falaichte"

#: ../src/app/qml/ui/ViewPopover.qml:37
msgid "View As"
msgstr "Seall mar"

#: ../src/app/qml/ui/ViewPopover.qml:39
msgid "List"
msgstr "Liosta"

#: ../src/app/qml/ui/ViewPopover.qml:39
msgid "Icons"
msgstr "Ìomhaigheagan"

#: ../src/app/qml/ui/ViewPopover.qml:44
msgid "Grid size"
msgstr ""

#: ../src/app/qml/ui/ViewPopover.qml:47 ../src/app/qml/ui/ViewPopover.qml:55
msgid "S"
msgstr ""

#: ../src/app/qml/ui/ViewPopover.qml:47 ../src/app/qml/ui/ViewPopover.qml:55
msgid "M"
msgstr ""

#: ../src/app/qml/ui/ViewPopover.qml:47 ../src/app/qml/ui/ViewPopover.qml:55
msgid "L"
msgstr ""

#: ../src/app/qml/ui/ViewPopover.qml:47 ../src/app/qml/ui/ViewPopover.qml:55
msgid "XL"
msgstr ""

#: ../src/app/qml/ui/ViewPopover.qml:52
#, fuzzy
msgid "List size"
msgstr "Liosta"

#: ../src/app/qml/ui/ViewPopover.qml:60
msgid "Sort By"
msgstr "Seòrsaich le"

#: ../src/app/qml/ui/ViewPopover.qml:62
msgid "Name"
msgstr "Ainm"

#: ../src/app/qml/ui/ViewPopover.qml:62
msgid "Date"
msgstr "Ceann-là"

#: ../src/app/qml/ui/ViewPopover.qml:62
#, fuzzy
msgid "Size"
msgstr "Meud:"

#: ../src/app/qml/ui/ViewPopover.qml:67
msgid "Sort Order"
msgstr "Òrdugh an t-seòrsachaidh"

#: ../src/app/qml/ui/ViewPopover.qml:74
msgid "Theme"
msgstr ""

#: ../src/app/qml/ui/ViewPopover.qml:76
msgid "Light"
msgstr ""

#: ../src/app/qml/ui/ViewPopover.qml:76
msgid "Dark"
msgstr ""

#: ../src/app/qml/views/FolderDelegateActions.qml:40
msgid "Folder not accessible"
msgstr "Cha ghabh am pasgan inntrigeadh"

#. TRANSLATORS: this refers to a folder name
#: ../src/app/qml/views/FolderDelegateActions.qml:42
#, qt-format
msgid "Can not access %1"
msgstr "Cha ghabh %1 inntrigeadh"

#: ../src/app/qml/views/FolderDelegateActions.qml:136
msgid "Could not rename"
msgstr "Cha b' urrainn dhut ainm ùr a thoirt air"

#: ../src/app/qml/views/FolderDelegateActions.qml:137
#, fuzzy
msgid ""
"Insufficient permissions, name contains special chars (e.g. '/'), or already "
"exists"
msgstr "'S dòcha gu bheil cead a dhìth ort no gu bheil an t-ainm ann mar-thà."

#: ../src/app/qml/views/FolderListView.qml:72
msgid "Directories"
msgstr ""

#: ../src/app/qml/views/FolderListView.qml:72
#, fuzzy
msgid "Files"
msgstr "%1 Faidhle"

#: ../src/plugin/folderlistmodel/dirmodel.cpp:360
msgid "Unknown"
msgstr "Chan eil fhios"

#: ../src/plugin/folderlistmodel/dirmodel.cpp:487
msgid "path or url may not exist or cannot be read"
msgstr "Dh’fhaoidte nach eil an t-slighe no URL ann no nach gabh a leughadh"

#: ../src/plugin/folderlistmodel/dirmodel.cpp:690
msgid "Rename error"
msgstr "Mearachd leis an ath-ainmeachadh"

#: ../src/plugin/folderlistmodel/dirmodel.cpp:713
msgid "Error creating new folder"
msgstr "Mearachd a’ cruthachadh a’ phàsgain ùir"

#: ../src/plugin/folderlistmodel/dirmodel.cpp:740
msgid "Touch file error"
msgstr ""

#: ../src/plugin/folderlistmodel/dirmodel.cpp:1353
msgid "items"
msgstr "nithean"

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:320
msgid "File or Directory does not exist"
msgstr "Chan eil am faidhle no pasgan ann"

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:321
msgid " does not exist"
msgstr " - chan eil seo ann"

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:326
msgid "Cannot access File or Directory"
msgstr "Cha ghabh am faidhle no am pasgan inntrigeadh"

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:327
msgid " it needs Authentication"
msgstr " tha e feumach air dearbhadh"

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:361
msgid "Cannot copy/move items"
msgstr "Cha ghabh lethbhreac a dhèanamh dhe na nithean/an gluasad"

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:362
#: ../src/plugin/folderlistmodel/filesystemaction.cpp:1487
msgid "no write permission on folder "
msgstr "chan eil cead sgrìobhaidh air a' phasgan seo ann: "

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:642
msgid "Could not remove the item "
msgstr "Cha b' urrainn dhuinn an nì seo a thoirt air falbh: "

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:680
msgid "Could not find a suitable name to backup"
msgstr ""
"Cha b’ urrainn dhuinn ainm iomchaidh a lorg airson lethbhreac-glèidhidh a "
"dhèanamh"

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:733
msgid "Could not create the directory"
msgstr "Cha b’ urrainn dhuinn am pasgan a chruthachadh"

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:738
msgid "Could not create link to"
msgstr "Cha b' urrainn dhuinn ceangal a chruthachadh gu"

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:746
msgid "Could not set permissions to dir"
msgstr "Cha b' urrainn dhuinn ceadan a' phasgain a shuidheachadh"

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:756
msgid "Could not open file"
msgstr "Cha b’ urrainn dhuinn am faidhle fhosgladh"

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:772
msgid "There is no space to copy"
msgstr "Chan eil rum ann gus lethbhreac a dhèanamh"

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:780
msgid "Could not create file"
msgstr "Cha b’ urrainn dhuinn am faidhle a chruthachadh"

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:837
msgid "Could not remove the directory/file "
msgstr "Cha b' urrainn dhuinn am pasgan/faidhle seo a thoirt air falbh: "

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:850
msgid "Could not move the directory/file "
msgstr "Cha b' urrainn dhuinn am pasgan/faidhle seo a ghluasad: "

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:1089
msgid "Write error in "
msgstr "Mearachd sgrìobhaidh ann an: "

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:1101
msgid "Read error in "
msgstr "Mearachd leughaidh ann an: "

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:1318
msgid " Copy"
msgstr " (lethbhreac)"

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:1371
msgid "Set permissions error in "
msgstr "Mearachd le suidheachadh nan ceadan airson "

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:1437
msgid "Could not create trash info file"
msgstr "Cha deach leinn faidhle fiosrachadh an sgudail a chruthachadh"

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:1449
msgid "Could not remove the trash info file"
msgstr "Cha deach leinn faidhle fiosrachadh an sgudail a thort air falbh"

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:1479
#: ../src/plugin/folderlistmodel/filesystemaction.cpp:1486
msgid "Cannot move items"
msgstr "Chan urrainn dhuinn na nithean a ghluasad"

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:1481
msgid "origin and destination folders are the same"
msgstr "Tha am pasgan tùsail is an ceann-uidhe co-ionnann"

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:1524
msgid "There is no space to download"
msgstr "Chan eil rum ann gus a luchdadh a-nuas"

#: ../src/plugin/folderlistmodel/smb/qsambaclient/src/smbusershare.cpp:72
msgid "net tool not found, check samba installation"
msgstr "Cha deach nettool a lorg, thoir sùil air an stàladh dhe samba agad"

#: ../src/plugin/folderlistmodel/smb/qsambaclient/src/smbusershare.cpp:80
msgid "cannot write in "
msgstr "cha ghabh sgrìobhadh an-seo: "

#: com.ubuntu.filemanager.desktop.in.in.h:1
msgid "File Manager"
msgstr "Manaidsear fhaidhlichean"

#: com.ubuntu.filemanager.desktop.in.in.h:2
msgid "folder;manager;explore;disk;filesystem;"
msgstr ""

#~ msgid "%1 (%2 file)"
#~ msgid_plural "%1 (%2 files)"
#~ msgstr[0] "%1 (%2 fhaidhle)"
#~ msgstr[1] "%1 (%2 fhaidhle)"
#~ msgstr[2] "%1 (%2 faidhlichean)"
#~ msgstr[3] "%1 (%2 faidhle)"

#~ msgid "Device"
#~ msgstr "Uidheam"

#~ msgid "Change app settings"
#~ msgstr "Atharraich roghainnean na h-aplacaid"

#~ msgid "password"
#~ msgstr "facal-faire"

#~ msgid "Path:"
#~ msgstr "Slighe:"

#~ msgid "Contents:"
#~ msgstr "Susbaint:"

#~ msgid "Unlock full access"
#~ msgstr "Fosgail làn-inntrigeadh"

#~ msgid "Enter name for new folder"
#~ msgstr "Cuir a-steach ainm a' phasgain ùir"

#~ msgid "~/Desktop"
#~ msgstr "~/An deasg"

#~ msgid "~/Public"
#~ msgstr "~/Poblach"

#~ msgid "~/Programs"
#~ msgstr "~/Prògraman"

#~ msgid "~/Templates"
#~ msgstr "~/Teamplaidean"

#~ msgid "Home"
#~ msgstr "Dachaigh"

#~ msgid "Network"
#~ msgstr "Lìonra"

#~ msgid "Go To Location"
#~ msgstr "Gach gun ionad"

#~ msgid "Enter a location to go to:"
#~ msgstr "Cuir a-steach ionad gu dol ann:"

#~ msgid "Location..."
#~ msgstr "Ionad..."

#~ msgid "Go"
#~ msgstr "Siuthad"

#~ msgid "Show Advanced Features"
#~ msgstr "Seall roghainnean adhartach"

#~ msgid "Ascending"
#~ msgstr "A' dìreadh"

#~ msgid "Descending"
#~ msgstr "A' teàrnadh"

#~ msgid "Filter"
#~ msgstr "Criathrag"
